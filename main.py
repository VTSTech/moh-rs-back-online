#!/usr/bin/env python3

import socket
import sys
import struct
import string
import multiprocessing

ip = "10.10.10.6"
port = 14300
	
MATCHMAKER_HEADER_SIZE = 12
MATCHMAKER_REQUESTS = {
	"CONN": "CONN",
	"USER": "USER",
	"FILE": "FILE",
	"FCHU": "FCHU",
	"PROF": "PROF",
	"LLST": "LLST",
	"LDAT": "LDAT",
	"GLST": "GLST",
	"GDAT": "GDAT",
	"RDAT": "RDAT",
	"RLST": "RLST",
	"UGAM": "UGAM",
	"CGAM": "CGAM",
	"DGAM": "DGAM",
	"RGAM": "RGAM",
	"KICK": "KICK",
	"MOTD": "MOTD",
}

VALID_ASCII_DATA = string.ascii_letters + string.digits + string.punctuation + " "

def DataToParameter(data):
	if(len(data) <= 0):
		return {}
	
	list = data.split(' ')
	para = {}
	
	for i, v in enumerate(list):
		item = v.split('=')
		
		if(item[0] != ""):
			para[item[0]] = item[1]
	
	return para

def DataToString(data):
	data_str = ""
	
	for d in data.decode("ascii"):
		if(d in VALID_ASCII_DATA):
			data_str += d
		else:
			data_str += "."
	
	return data_str

def ConnToAdress(conn):
	peername = conn.getpeername()
	return "{}:{}".format(peername[0], peername[1])

def makeResponse(cmd, para):
	data_size = MATCHMAKER_HEADER_SIZE + len(para) + 1
	
	## Matchmaker command
	response = struct.pack("{}s".format(len(cmd)), bytes(cmd, 'ascii'))
	
	## Empty space
	response += struct.pack(">i", 0)
	
	## Data size
	response += struct.pack(">i", data_size)
	
	## Parameters
	response += struct.pack("{}s".format(len(para)), bytes(para, 'ascii'))
	
	## End sign
	response += struct.pack("c", bytes("\0", 'ascii'))
	
	return response

def processData(conn, data):
	cmd = data[:4].decode("ascii")
	
	if cmd in MATCHMAKER_REQUESTS:
		para = DataToParameter(data[MATCHMAKER_HEADER_SIZE:-1].decode("ascii"))
		
		## Get function name
		func_name = MATCHMAKER_REQUESTS[cmd]
		
		if(func_name in globals()):
			## Get command function
			func = globals()[func_name]
		
			## Execute function
			func(conn, data, para)
		#else:
			#print("Warning: function name \"{}\" hasn't been implemented.".format(func_name))
		
## Server Commands ##
def CONN(conn, data, para):
	response = makeResponse("CONN", "")
	
	conn.send(response)
	
	print("{} -> {}".format(ConnToAdress(conn), DataToString(response)))
	
def USER(conn, data, para):
	response = makeResponse("USER", "TICKET={}".format("YOLO"))
	#response = makeResponse("USER", "{}".format("band"))
	conn.send(response)
	
	print("{} -> {}".format(ConnToAdress(conn), DataToString(response)))

def LLST(conn, data, para):
	response = makeResponse("LLST", "TID={} NUM-LOBBIES={}".format(para["TID"], 1))
	conn.send(response)
	
	print("{} -> {}".format(ConnToAdress(conn), DataToString(response)))
	
	response = makeResponse("LDAT", "TID={} LOBBY-ID={} NAME=\"{}\" LOCALE={} NUM-GAMES={} FAVORITE-GAMES={} FAVORITE-PLAYERS={}".format(
		para["TID"], 0, "IamLupo", 0, 1337, 0, 0))
	conn.send(response)
	
	print("{} -> {}".format(ConnToAdress(conn), DataToString(response)))

def GLST(conn, data, para):
	response = makeResponse("GLST", "TID={} NUM-GAMES={} LOBBY-ID={}".format(para["TID"], 1, para["LOBBY-ID"]))
	conn.send(response)
	
	print("{} -> {}".format(ConnToAdress(conn), DataToString(response)))
	
	response = makeResponse(
		"GDAT",
		"TID={} IP=\"{}\" PORT={} GAME-ID={} FAVORITE={} NUM-FAV-PLAYERS={} NUM-PLAYERS={} MAX-PLAYERS={} NAME=\"{}\"".format(
		#para["TID"], "10.10.10.8", 28500, 0, 0, 0, 1, 8, "IamLupo"))
		para["TID"], "86.80.93.9", 28500, 0, 1, 0, 1, 8, "IamLupo"))
	conn.send(response)
	
	print("{} -> {}".format(ConnToAdress(conn), DataToString(response)))

def FILE(conn, data, para):
	pass

def PROF(conn, data, para):
	response = makeResponse("PROF", "CLEAN-TEXT=\"YOLO\"")
	conn.send(response)
	
	print("{} -> {}".format(ConnToAdress(conn), DataToString(response)))

def RLST(conn, data, para):
	response = makeResponse("RLST", "TID={} NUM-REGIONS={}".format(para["TID"], 1))
	conn.send(response)
	
	print("{} -> {}".format(ConnToAdress(conn), DataToString(response)))
	
	response = makeResponse(
		"RDAT",
		"TID={} REGION-ID={} NAME=\"{}\" LOCALE={} NUM-GAMES={} NUM-PLAYERS={}".format(
		para["TID"], 1, "IamLupo", 0, 1, 1))
	conn.send(response)
	
	print("{} -> {}".format(ConnToAdress(conn), DataToString(response)))

def listen_client(conn):
	try:
		print("client connected: {}".format(ConnToAdress(conn)))
		
		while True:
			data = conn.recv(200)
			
			if(len(data) <= MATCHMAKER_HEADER_SIZE):
				break
			
			print("{} <- {}".format(ConnToAdress(conn), DataToString(data)))
			
			processData(conn, data)
	finally:
		print("Client {} disconnected.".format(ConnToAdress(conn)))
		conn.close()

def banner():
	print("moh-rs-back-online written by IamLupo")
	print("Fork by VTSTech\n")

def start_server(ip, port):
	banner()
	print("starting up server")
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	sock.bind((ip, port))
	sock.listen(1000)
	print("bind complete "+ip+":"+str(port))
	print('waiting for a connections')
	
	while True:
		conn, adress = sock.accept()
		
		## Create listen thread
		multiprocessing.Process(target=listen_client, args=[conn]).start()

def main(argv):
	global port, ip
	for i, v in enumerate(argv):
		if(i + 1 < len(argv)):
			## IP parameter
			if(v == "-ip"):
				ip = argv[i + 1]
			
			## Port parameter
			elif(v == "-port"):
				port = int(argv[i + 1], 10)
	
	start_server(ip, port)

if __name__ == "__main__":
	main(sys.argv[1:])
